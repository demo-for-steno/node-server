# node-server

This is a Node/TypeScript server that I am building to learn these technologies.

Initial project setup followed this tutorial: https://khalilstemmler.com/blogs/typescript/node-starter-project/.

This was a useful reference for NVM and Node on WSL: https://learn.microsoft.com/en-us/windows/dev-environment/javascript/nodejs-on-wsl
