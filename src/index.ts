#!/usr/bin/env node
import express from 'express';
import fs from 'fs';
import path from 'path';

const application = express();

var projectRoot = getProjectRoot();

var pageContents = fs.readFileSync(projectRoot + '/index-content.htm', 'utf8');
var styles = fs.readFileSync(projectRoot + '/styles.css', 'utf8');

application.get('/api/info', (request, response) => {
  response.setHeader('Content-Type', 'text/html');
  var result: string = 
	  '<html>' +
	  '<head>' +
	  '  <title>Node.js/TypeScript/React/PostgreSQL learning project</title>' +
	  '  <style>' + styles + '</style>' +
	  '</head>' +
	  '<body>' +
	  pageContents +
	  '</body>' +
	  '</html>';
  response.send(result);
})

// Get the port from the environment.
var port: number = 0;
if (typeof process.env.PORT === 'string') {
  port = Number.parseFloat(process.env.PORT);
}

// If the port is not valid, default to 8080.
if (port < 1 || port > 65535 || !Number.isInteger(port)) {
  port = 8080;
  console.log('set port to default of ' + port);
}
console.log('port is ' + port);

// Start the listener.
application.listen(port, () => {
  console.log('started listener on port ' + port);
});

// Attempts to find the working directory, regardless of run mode.
function getProjectRoot() {
  // Get the absolute path of the current directory.
  var root: string = path.resolve('.');

  if (fs.existsSync(root + '/src')) {
    root = root + '/src/';
  }
  console.log('project root: ' + root);
  return root;
}
